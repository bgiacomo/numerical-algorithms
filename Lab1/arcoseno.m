function [ P ] = leibnizsen( n )
%metodo li leibniz con arcoseno
tic;
Q(1)=0;
T(1)=1/2;

for k=1:n
    Q(k+1)=Q(k)+(T(k))/((2*k)-1);
    T(k+1)=(T(k)*(2*k-1))/(8*k);

end
P=6*Q(n);
toc;

