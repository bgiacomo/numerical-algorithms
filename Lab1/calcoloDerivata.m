clear all
close all
clc

x=1;

h=zeros(16,1) %da 10^-1 a 10^-16

for i=1:1:16
    h(i)=10^(-i); % vettore di h 10^-i
end

y1_preciso=funzione1(x);
fprintf('H\t  |  Valore approssimato\t  |  Valore esatto\t | Err');
for i=1:length(h)
    y1_appr(i)=derivata(@funz1,x,h(i));
    err_relativo(i)=abs(y1_appr(i)-y1_preciso)/abs(y1_preciso);
    fprintf('\n%e\t%14.15e\t%14.15e\t%14.15e\n',h(i),y1_appr(i),y1_preciso,err_relativo(i));
end
