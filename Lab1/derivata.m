function y=derivata(funz,x,h)

y=((funz(x+h)-funz(x))/h);

end
