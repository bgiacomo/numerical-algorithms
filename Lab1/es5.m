clear all
close all
clc

A=[1,-6,15,-20,15,-6,1];

scelta=input('Scegli: polinomio(1), polinomio2(2) o hoerner(3)?');

if scelta==1
    %plotto con polinomio
    X=linspace(1-0.1,1+0.1,1000);
    for i=1: length(X)
        Y(i)=polinomio(A,X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.01,1+0.01,1000);
    for i=1: length(X)
        Y(i)=polinomio(A,X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.008,1+0.008,1000);
    for i=1: length(X)
        Y(i)=polinomio(A,X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.007,1+0.007,1000);
    for i=1: length(X)
        Y(i)=polinomio(A,X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.005,1+0.005,1000);
    for i=1: length(X)
        Y(i)=polinomio(A,X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.003,1+0.003,1000);
    for i=1: length(X)
        Y(i)=polinomio(A,X(i));
    end
    
    figure
    plot(X,Y);
    
end

if scelta==2
      %plotto con polinomio
    X=linspace(1-0.1,1+0.1,1000);
    for i=1: length(X)
        Y(i)=polinomio2(X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.01,1+0.01,1000);
    for i=1: length(X)
        Y(i)=polinomio2(X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.008,1+0.008,1000);
    for i=1: length(X)
        Y(i)=polinomio2(X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.007,1+0.007,1000);
    for i=1: length(X)
        Y(i)=polinomio2(X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.005,1+0.005,1000);
    for i=1: length(X)
        Y(i)=polinomio2(X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.003,1+0.003,1000);
    for i=1: length(X)
        Y(i)=polinomio2(X(i));
    end
    
    figure
    plot(X,Y);
    
end

if scelta==3
      %plotto con polinomio
    X=linspace(1-0.1,1+0.1,1000);
    for i=1: length(X)
        Y(i)=hoerner(A,X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.01,1+0.01,1000);
    for i=1: length(X)
        Y(i)=hoerner(A,X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.008,1+0.008,1000);
    for i=1: length(X)
        Y(i)=hoerner(A,X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.007,1+0.007,1000);
    for i=1: length(X)
        Y(i)=hoerner(A,X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.005,1+0.005,1000);
    for i=1: length(X)
        Y(i)=hoerner(A,X(i));
    end
    
    figure
    plot(X,Y);
    
    X=linspace(1-0.003,1+0.003,1000);
    for i=1: length(X)
        Y(i)=hoerner(A,X(i));
    end
    
    figure
    plot(X,Y);
    
end