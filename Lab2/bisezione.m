function [c] = bisezione(funzione,a,b,prec,it)

i=0;

while(b-a>prec && i<it)
    
    fa=feval(funzione,a);
    fb=feval(funzione,b);
    
    if(fa*fb<0)
        c=(a+b)/2;
        fc=feval(effe,c);
        if(fc==0)
            return;
        else
            if(fa*fc<0)
                b=c;
            else
                a=c;
            end
        end
        
    end
    
    i=i+1;
    info(1)=c;
    info(2)=i;
    info(3)=b-a;
end

fprintf('Bisez: 0 in x=%30f\nprec=%0.30f\nnum it=%d\n\n',info(1),info(3),info(2));




