clear all
clc 
close all



intervallo = input ('Inserisci l''intervallo [a,b] nel quale disegnare la funzione = ');

x = linspace(intervallo(1), intervallo(2), 1000);

for i=1: length(x)
    %Sostituire effe con la funzione che si vuole graficare
   y(i)=funzione(x(i));    
end

plot(x,y);
grid on;


intervallo = input ('Inserisci l'' intervallo [a,b] nel quale calcolare lo 0 = ');

bisezione(@funzione,intervallo(1),intervallo(2),10^(-5),30);
regula_falsi(@funzione,intervallo(1),intervallo(2),10^(-5),30);
%newton(@effe32,@effe32_der,abs(intervallo(1)+intervallo(2))/2,10^(-5),30);
secanti(@funzione,intervallo(1),intervallo(2),abs(intervallo(1)+intervallo(2))/2,10^(-5),30);