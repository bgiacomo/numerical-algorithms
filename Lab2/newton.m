function[info] = newton (funzione,der, x0, prec, it)

fx0=feval(funzione,x0);

i=1;

x0prec=eps;

f1x0=feval(der,x0);

while(abs(x0-x0prec)>prec && i<it)
    m=x0-(fx0/f1x0);
    x0prec=x0;
    x0=m;
    
    fx0=feval(effe,x0);
    f1x0=feval(der,x0);
    
    i=i+1;
    
end

info(1)=x0;
info(2)=i;
info(3)=abs(x0-x0prec); %questo � l'errore

fprintf('Newton: 0 in x=%0.30f\nprec=%0.30f\nnum iterazioni= %d\n\n',info(1),info(3),info(2));