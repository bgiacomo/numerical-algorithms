function [ info ] = regula_falsi(funzione,a,b,prec, it )
%metodo regula falsi
i=1;
while(abs(b-a)>prec && i<it)
    
    fx0=feval(funzione,a);
    fx1=feval(funzione,b);
    
    x2(i)=b-(fx1*(b-a))/(fx1-fx0);
    fx2(i)=feval(funzione,x2(i));
    
    if(fx2(i)>0)
        err1=b;
        b=x2(i);
    else
        err1=a;
        a=x2(i);
    end
    c=x2(i);
    i=i+1;
end
err=a-err1;
info(1)=c;
info(2)=i;
info(3)=abs(err);

fprintf('regula: 0 in x=%0.30f\nprec=%0.30f\niterazioni=%d\n\n',info(1),info(3),info(2));
   
