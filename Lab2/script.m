clear all
close all
clc


fprintf('Inserisci i parametri: 1)a 2)b 3)prec 4)it');
%f=input('\n1:','s'); 
a=input('\na: ');
b=input('\nb:');
prec=input('\nprec: ');
it=input('\nit: ');
array2=linspace(a,b,500);
array=linspace(a,b,500); %linspace crea l'array

fprintf('\nInserisci l''intervallo in cui ricercare lo zero: ');
a=input('a: ');
b=input('b: ');
x=a;
fa=funzione(a);
x=b;
fb=funzione(b);
while(fa*fb>0)
    fprintf('\nIntervallo errato, reinseriscilo: ');
    a=input('a: ');
    b=input('b: ');
    x=a;
fa=funzione(a);
x=b;
fb=funzione(b);
end

for(i=1:500)
   x=array(i);
   array(i)=funzione(x);
end
c=bisezione(a,b,prec,it);
fprintf('Radice con bisezione: %d',c);
c=regula_falsi(a,b,prec,it);
fprintf('\n\nRadice con regula_falsi: %d',c);
fprintf('\nInserire funzione per metodo Newton');
effe=input('\f: ');
fprintf('\n\nInserire x0 per Newton: ');
x0=input('x0: ');
fprintf('\nInserire numero iterazioni max: ');
it=input('nIt: ');
fprintf('\nInserire la derivata: ');
der=input('derivata:');
fprintf=('\nInserire la precisione di calcolo:');
prec=input('prec= ');
e=newton(effe,der, x0, prec, it)
fprintf('\n Radice secondo Newton: %d',e);
%fprintf('\nMargine di errore: %d',e(3));
figure;
plot(array2,array);