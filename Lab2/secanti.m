function[] = secanti (funzione, a,b,x0, prec, it)
%METODO PER TROVARE GLI ZERI CON LA SECANTE

i=1;
m=abs(a+b)/2;
fm=feval(funzione,m);
fa=feval(funzione,a);

while(i<2)
    fx0=feval(funzione,a);
    fx1=feval(effe,b);
    x2(i)=b-(fx1*(b-a))/(fx1-fx0);
    fx2(i)=feval(funzione,x2(i));
    if(fx2(i)>0)
        b=x2(i);
    else
        a=x2(i);
    end
    c=x2(i);
    i=i+1;

end
fx0=funzione(x0);
m=a-(fa/((fa-fx0)/(a-x0)));
errore=abs(x0-m);

while(errore>prec && i<it)
    x0=a;
    a=m;
    fx0=feval(funzione,x0);
    fa=feval(funzione,a);
    m=a-(fa/((fa-fx0)/(a-x0)));
    i=i+1;
    
    errore=abs(x0-m);
end
info(1)=m;
info(2)=i;
info(3)=errore;
fprintf('Secanti: 0 in x=%0.30f\nprec=%0.30f\nnum iterazioni=%d\n\n',info(1),info(3),info(2));
