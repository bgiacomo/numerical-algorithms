function[nor]=norma(A,flag)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[m,n]=size(A);

% n=num colonne
% m=num righe
nor=0;

array_colonne=zeros(n,1);  % dichirazioen array inizializzando gli elementi a zero
array_righe=zeros(m,1);
if (flag==1)
    for j=1:n 
        for i=1:m
             array_colonne(j)=array_colonne(j)+abs(A(i,j));
        end
    end
    
nor=max(array_colonne);
end
if (flag==0)
      
 for j=1:m 
        for i=1:n
             array_righe(i)=array_righe(i)+abs(A(i,j));
        end
    end
nor=max(array_righe);
end
end
