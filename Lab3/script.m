clear all
close all
clc

%pongo di default n=12 %

A=wilk(12) %wilkinson
spy(A) %visualizzo struttura matrice
[L,R,P]=fatt_lu( A ); %gauss

Rp=R;

elem =(R(12,12)/100)*5; %calcolo la perturbazione
Rp(12,12)=R(12,12)+elem; %lo sommo all'elemento in R per ottenere l'elem perturbato
Rp

Ap=L*Rp;
%calcolo l'errore %
error=abs(Ap(12,12)-A(12,12))/abs(A(12,12))





