clear all
close all
clc

fprintf('N \t | Errore \t| Cond \t|\n');

for(n=2:15)
    x=zeros(1,n);
    vettReale=ones(1,n); %creo il vettore di uno soluz.reali
    A=vander(1,100,n); %creo la matrice di vandermonde: i parametri mi creano il vettore da 1 a 100 di passo n!
    b=zeros(1,n); 
for i=1:n
    for j=1:n
        b(i)=A(i,j)+b(i);
    end
end
    x=risolvi_sistema(A,b);
    c=x-vettReale; %c sarebbe la soluz computer - soluz reale
    err1=normaVett(c,1); %calcolo la norma1 di c
    err2=normaVett(vettReale,1); %calcolo la norma1 delle soluz reali
    errore=err1/err2; %calcolo l'errore relativo
    fprintf('%d\t%e\t%e\n',n,errore,cond(A)); %il condizionamento lo calcolo direttamente in stampa
end
 