clear all
close all
clc


A=hilb(10)+eye(10);
b=sum(A,2) ;

cond=cond(A,1);
x=risolvi_sistema(A,b);
%calcolo della soluz. perturbata
b(1)=b(1)*0.01+b(1);
xp=risolvi_sistema(A,b);

%errore relativo
nor=normaVett(x-xp,0)/normaVett(x,0);

fprintf('indice di condizionamento = %d\n errore relativo=%d\n',cond,nor);