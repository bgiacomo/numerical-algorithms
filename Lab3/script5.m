clear all
close all
clc

A=[10,1,4,0;1,10,5,-1;4,5,10,7;0,-1,7,9];
b=[15;15;26;15];

%sistema senza perturbazione
cond=cond(A,1);
x=risolvi_sistema(A,b);

%calcolo della soluzione perturbata
b(1)=b(1)*0.01+b(1);
xp=risolvi_sistema(A,b);

%errore relativo
err=normaVett(x-xp,0)/normaVett(x,0);

fprintf('Errore relativo= %d\n',err);
fprintf('Soluz. perturbata= %d\n',xp);
fprintf('Soluz. non perturbate=%d\n',x);