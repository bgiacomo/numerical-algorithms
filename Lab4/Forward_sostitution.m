function [ x ] = Forward_sostitution (L,b)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

[m,n]=size(L);

x=zeros(m,1);

for i=1:n
    x(i)=b(i);
   for j=1:i-1
   x(i)=x(i)-L(i,j)*x(j);
   end
   x(i)=x(i)/L(i,i);
end

end

