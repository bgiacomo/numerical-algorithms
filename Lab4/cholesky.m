function [ L,flag ] = cholesky( A )

[n,n] = size(A);
L = zeros(n,n);
for j=1:n
        somma=sum(L(j,1:j-1).^2);
        if (A(j,j)-somma) < 0
            flag=-1;
            return
        else
            L(j,j) = sqrt(A(j,j)- somma);
            for i = j+1:n
                L(i,j)= (A(i,j)- sum(L(j,1:j-1).*L(i,1:j-1)))/L(j,j);
            end
        
        end
end
flag=1;
