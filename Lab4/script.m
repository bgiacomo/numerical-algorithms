clear all
close all
clc

n=[10,50,100,250,1000,1500];

for i=1:6
    j=n(i);
fprintf('n=%d\n',j);
a=2*ones(j,1);
b=ones(j-1,1);
c=ones(j-1,1);

A=diag(a)+diag(b,-1)+diag(c,1);

t=sum(A,2);
tic;
xGauss=risolvi_sistema(A,t);
toc
%fprintf('tempo di calcolo per n = %d',n(i));
%fprintf('\nFatt. Gauss:');

b=[0;b];
tic;
xTridiag=tridiagGauss(a,b,c,t);
%fprintf('Gauss tridiag.');
toc
fprintf('--------------------------------------\n');

end
