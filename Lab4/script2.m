clear all
close all
clc

%la matrice simmetrica positiva la metto manualmente sfruttando la
%condizine sufficiente percui se A (SIMMETRICA) � a diag. dom. con gli elementi > 0
%allora � simm.positiva:
n=300;
A=rand(n);
B=A'*A;

[L,flag]=cholesky(B);
if(flag==1)
    %qui dentro cholesky � possibile e la matrice � simm. positiva%
    %mi creo il vettore y in maniera tale che la soluz sia unitaria
    
    y=zeros(1,n);
    x=zeros(1,n);
for i=1:n
    for j=1:n
    y(i)=y(i)+B(i,j);
    end
end
fprintf('C: \n');
tic;
L=cholesky(B);
y1=Forward_sostitution(L,y);
x=Backward_sostitution(L',y1)
toc
fprintf('G: \n');
tic;
x=risolvi_sistema(B,y)
toc
end

if(flag==-1)
 fprintf('impossibile fare cholesky\n');   
end





