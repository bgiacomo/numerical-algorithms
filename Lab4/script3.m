clear all
close all
clc

%la matrice simmetrica positiva la metto manualmente sfruttando la
%condizine sufficiente percui se A � a diag. dom. con gli elementi > 0
%allora � simm.positiva:

A=[5,1,0,2;1,7,2,1;0,2,7,0;2,1,0,9];

%per avere il vettore unitario sommo gli elementi delle righe per ottenere
%b

b=zeros(1,4);

for i=1:4
    for j=1:4
    b(i)=b(i)+A(i,j);
    end
end

L=cholesky(A);
y=Forward_sostitution(L,b);
x=Backward_sostitution(L',y)














