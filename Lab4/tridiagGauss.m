function [x ] =  tridiagGauss( a,b,c, t)

n=length(a);

alpha(1)=a(1);


for i=2:n
    %moltiplicatore
    beta(i)=b(i)/alpha(i-1);
    alpha(i)=a(i)-beta(i)*c(i-1);
end

%Ly=t, Rx=y

%sostituzione in avanti per Ly=t
y(1)=t(1);
for i=2:n
    y(i)=t(i)-beta(i)*y(i-1);
end
%sostituzione indietro Rx=y
x(n)=y(n)/alpha(n);
for i=n-1:-1:1
    x(i)=(y(i)-c(i)*x(i+1))/alpha(i);
end




