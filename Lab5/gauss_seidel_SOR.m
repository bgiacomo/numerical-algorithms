function[xnew,err,k,rho]=gauss_seidel_SOR(A,b,x0,omega,maxiters,prec)
%metodo di iterazione di Gauss-Seidel per risolvere sistemi lineari

%step1: scompongo A in D,E,F

D=diag(diag(A));
E=tril(A,-1);
F=triu(A,1);

%step2: per th.Gauss-Seidel SOR si ha che M=D+omega*E; N=((1-omega)*D-omega*F)

M=D+omega*E;
N=((1-omega)*D-omega*F);

%step3: calcolo matrice di iterazione, raggio spettrale rho.
%NB: se rho>1 allora Gauss-seidel non funziona!

T=inv(M)*N;
T=inv(M)*N;
rho=abs(eigs(T,1,'lm'));

if rho>=1
    disp('Errore!');
%     xnew=-2;
%     err=-2;
%     k=-2;
%     rho=-2;
%     return;
end

%step4: algoritmo di Gauss-Seidel:

arresto=1;
k=1;
xold=x0;
q=inv(M)*b;

while arresto>=prec && k<=maxiters
    xnew=T*xold+q;
    arresto=norm(xnew-xold,1)/norm(xnew,1);
    err(k)=arresto;
    xold=xnew;
    k=k+1;
end

k=k-1;
err
k
rho

end
