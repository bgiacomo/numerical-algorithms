function[xnew,err,k,rho]=jacobi(A,b,x0,maxiters,prec)
%metodo di iterazione di jacobi per risolvere sistemi lineari

%step1: scompongo A in D,E,F

D=diag(diag(A));
E=tril(A,-1);
F=triu(A,1);

%step2: per Jacobi si ha che M=D ed N=-(E+F)

M=D;
N=-(E+F);

%step3: costruisco la mia matrice di iterazione T e calcolo il raggio spettrale:

T=inv(M)*N;
rho=abs(eigs(T,1,'lm'));

if rho>=1
    disp('Errore! Raggio spettrale > di 1.');
    xnew=-2;
    err=-2;
    k=-2;
    rho=-2;
    return;
end


%step4: inizializzo il processo iterativo:

arresto=1;
k=1;
xold=x0;
q=inv(M)*b;

%xnew=vettore delle nuove soluzioni
%xold=vettore delle precedenti soluzioni

while arresto>=prec && k<=maxiters
    xnew=T*xold+q;
    arresto=norm(xnew-xold,1)/norm(xnew,1);
    err(k)=arresto;
    xold=xnew;
    k=k+1;
end

k=k-1;
err
k
rho




