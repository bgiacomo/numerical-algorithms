clear all
close all
clc

maxiters=3000;
prec=10^-5;
scelta=input('Scegli matrice A,B,C,D,E: ','s');
metodo=input('Scegli il metodo di risoluzione: 1)Jacobi 2)Gauss Seidel 3)Gauss Seidel SOR: ');
if (scelta=='A')
    A=[3 0 4;7 4 3; -1 -1 2]
end
if (scelta=='B')
    A=[5 0 -1 2; -2 4 1 0;0 -1 4 -1;2 0 0 3]
end
if (scelta=='C')
   A=[3 -1 0 0 0 -1;-1 3 -1 0 -1 0;0 -1 3 -1 0 0;0 0 -1 3 -1 0;0 -1 0 -1 3 -1;-1 0 0 0 -1 3]
end
if (scelta=='D')
    %creo la matrice matrice tridiagonale di ordine 10, 
    %con elementi diagonali uguali a 4, elementi della codiagonale superiore uguali a -1, 
    %elementi della codiagonale inferiore uguali a -1.
    diagA=-ones(9,1);
    diagB= 4*ones(10,1);
    diagC=-ones(9,1);
    A=diag(diagB)+diag(diagC,-1)+diag(diagA,1)
end
if (scelta=='E')
    %creo la matrice tridiagonale di ordine 10, 
    %con elementi diagonali uguali a 2, elementi della codiagonale superiore uguali a -1, 
    %elementi della codiagonale inferiore uguali a -1.
    diagA=-ones(9,1);
    diagB= 2*ones(10,1);
    diagC=-ones(9,1);
    A=diag(diagB)+diag(diagC,-1)+diag(diagA,1)
end

b=sum(A,2)
[n,m]=size(A);
x0=zeros(n,1)

if(metodo==1)
        
  [xJac,errJac,kJac,rhoJac]=jacobi(A,b,x0,maxiters,prec) 
  figure
  plot(1:kJac,errJac)
end
if (metodo==2)
    [xGauss,errGauss,kGauss,rhoGauss]=gauss_seidel(A,b,x0,maxiters,prec)
    figure
    plot(1:kGauss,errGauss)
end
if (metodo==3)
    %il codice seguente ripete piu volte il metodo di Gauss Seiden SOR per
    %stabilire quale sia l'omega e il numero di iterazioni ottimale. Poi
    %grafica tutto.
    conta=1;
    for k=0:0.001:0.5
        omega(conta)=1+2*k;
        [xnew,err,kGaussSOR(conta),rhoGaussSOR(conta)]=gauss_seidel_SOR(A,b,x0,omega(conta),maxiters,prec);
        conta=conta+1;
    end
    figure
    plot(omega,rhoGaussSOR)
    figure
    plot(omega,kGaussSOR);
end

%se voglio utilizzare il metodo con rilassamento le seguenti due righe non
%devono essere commentate e viceversa!!

[min,posMin]=min(rhoGaussSOR);
omegaFinale=omega(posMin) %questo � il mio omega ottimo!

%Ora che ho trovato l'omega ottimale lo uso per Gauss Seidel SOR

%Anche quest'ultima riga di codice deve essere commentata in caso non si
%utilizzi Gauss_seidel_SOR!
[xGaussSOR,errGaussSOR,kGaussSOR,rhoGaussSOR]=gauss_seidel_SOR(A,b,x0,omegaFinale,maxiters,prec);






