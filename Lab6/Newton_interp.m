function [ Pn ] = Newton_interp(xval,x,d)
%Calcolo polinomio interpolatore secondo metodo newton
Pn=d(length(x));

for i=length(x)-1:-1:1
   Pn=d(i)+(xval-x(i))*Pn; 
end

end

