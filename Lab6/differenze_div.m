function [ d ] = differenze_div( x,y )
%Calcolo le differenze da usare in Newton

%d=zeros(length(x));

for k=0:length(x)-1
    d(k+1)=y(k+1);
end

for i=1:length(x)-1
   for k=(length(x)-1):-1:i
        d(k+1)=(d(k+1)-d(k))/(x(k+1)-x(k+1-i));
   end
end

end

