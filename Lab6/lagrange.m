function [ y_lagrange ] = lagrange(x,y,xval)
%Algoritmo per calcolo polinomio interpolatore lagrange in un punto
y_lagrange=0;
for i=1:length(x)
    product=y(i); %E' un passo intermedio, la teniamo come var locale
    for j=1:length(x)
        if(i~=j)
            product=product*(xval-x(j))/(x(i)-x(j));
        end
       
    end
     y_lagrange=y_lagrange+product;
end

end

