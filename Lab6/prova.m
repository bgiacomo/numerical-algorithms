function [ fx ] = prova(a,b, effe )
%
% Altro modo per passare una funzione: nel file m devo dichiarare un vettore di x,
% per forza di x. Nella funzione devo dargli in input effe, che va scritta fra apici
% e col . dopo la x:
% Es: prova(1,4,'x.^3') per x^3
%     prova(1,5,'x.*2') per 2x
% 
% fx � il vettore delle immagini e pu� avere il nome che vuole
%
%
x=[a:1:b]
fx=eval(effe);

end

