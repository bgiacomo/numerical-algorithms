clear all
close all
clc

k=[0:.5:4];
xval=[0:.001:4];
y=sin((pi*k)/4);

%fprintf('Scegliere il metodo di interpolazione: 1.Lagrange 2.Newton 3.Spline' );
scelta=input('Scegliere il metodo di interpolazione: 1.Lagrange 2.Newton 3.Spline 4.Confronto\n' );

if scelta==1
for i=1:length(xval)
    ylagrange(i)=lagrange(k,y,xval(i));
end
%grafico l'output%
figure;
plot(xval,ylagrange);
hold on
plot(k,y,'ro')
end

if scelta==2
d=differenze_div(k,y);
for i=1:length(xval)
    ynewton(i)=Newton_interp(xval(i),k,d);
end

%grafico l'output%
figure;
plot(xval,ynewton);
hold on
plot(k,y,'ro')
end

if scelta==3

for i=1:length(xval)
    yval(i)=spline(k, y, xval(i)); 
end

%grafico l'output%
figure;
plot(xval,yval);
hold on
plot(k,y,'ro')
end

if scelta==4
%lagrange
fprintf('Tempo lagrange: ')
tic;
for i=1:length(xval)
    ylagrange(i)=lagrange(k,y,xval(i));
end
toc

%grafico l'output%
figure;
plot(xval,ylagrange);
hold on
plot(k,y,'ro')


%newton
fprintf('Tempo newton: ')
tic;
d=differenze_div(k,y);
for i=1:length(xval)
    ynewton(i)=Newton_interp(xval(i),k,d);
end
toc

figure;
plot(xval,ynewton);
hold on
plot(k,y,'ro')

%spline
fprintf('Tempo spline: ')
tic;
    yval=spline(k, y, xval); 

toc

%grafico l'output%
figure;
plot(xval,yval);
hold on
plot(k,y,'ro')    

end


