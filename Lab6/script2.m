clear all
close all
clc

[x, y] = ginput;
xval=input('Intervallo xval: ');

opzione=input('Scegli algoritmo:\n1.Lagrange - 2.Newton - 3.Spline - 4.Confronto : ');

%trovo t

t(1)=0;
for i=2:length(x)
    t(i)=t(i-1)+sqrt((x(i)-x(i-1))^2+(y(i)-y(i-1))^2);
end
max=t(length(t));
t=t./max; %normalizzo la t facendo fratto il max


%Lagrange

if opzione==1 || opzione==4
    tic;
    for i=1:length(xval)
    
        xL(i)=lagrange( t,x,xval(i) ); 
        yL(i)=lagrange( t,y,xval(i) );
       
    end
    tempo_lagrange=toc;
    
    fprintf('Algoritmo di Lagrange: %d\n',tempo_lagrange);%stampa tic-toc
    
end


%Newton

if opzione ==2 || opzione==4
    tic;
    dx= differenze_div( t,x ); %Differenze divise
    dy=differenze_div(t,y);
    for i=1:length(xval)
    
        xN(i) = Newton_interp( xval(i),t, dx); 
        xN(i) = Newton_interp( xval(i),t,dy ); 
        
    end
    
   tempo_newton=toc; 

   fprintf('Algoritmo di Newton: %d\n',tempo_newton);
end


%Spline

if opzione==3 || opzione==4
    tic;
    yS=spline(t,y,xval);
    xS=spline(t,x,xval);
    tempo_spline=toc;   
    fprintf('Spline: %d\n',tempo_spline);
end
    

%Stampa
if opzione==1
    figure
    plot(xL,yL);
    title('Lagrange')
end
if opzione==2
    figure
    plot(xN,xN);
    title('Newton interp')
end
    
if opzione==3
    figure
    plot(xS,yS);
    title('Spline');
end

if opzione==4
    title('Confronto');
    plot(xL,yL,'r');
    hold on
    plot(xN,xN);
    hold on
    plot(xS,yS,'g');
end
hold on
plot(x,y,'ro')