clear all
close all
clc

%n=input('Scegliere n: ');
n=10;
opzione=input('Punti equidistanti=1 \nChebishev=2\n');

m=1000;
xval=linspace(-1,1,m);

if (opzione==1)
x=linspace(-1,1,n);
   
end

if (opzione==2)
    for i=0:n-1
        x(i+1)=cos((1+2*i)/(2*(n+1))*pi); 
    end
end

opzione2=input('Scegliere i punti di interpolazione: 1.1/1+25x^2 \n 2.|x|\n');

y=zeros(size(x));
if opzione2==1
    for i=1:n
        y(i)=1/(1+(25*x(i).^2));
    end
    yvera=1./(1+25*xval.^2);
end
if opzione2==2
    for i=1:n
       y(i)=abs(x(i)); 
    end
    yvera=abs(xval);
end


fprintf('Calcolo con metodo Lagrange')

hold on

    tic;
    for i=1:length(xval)
       yL(i)=lagrange(x,y,xval(i));
    end
    tLagrange=toc
    figure
    plot(xval,yL)
    title('Lagrange');
 
fprintf('\nCalcolo con metodo Newton')
tic; %NEL CALCOLO DEL TEMPO CONSIDERO ANCHE IL CALCOLO DELLE DIFFERENZE
dx=differenze_div( x,y );
    for i=1:length(xval)
        newton(i)=Newton_interp(xval(i),x,dx);
    end
    tNewton=toc
    figure
    plot(xval,newton)
    title('Newton');
 
    %calcolo tempo spline
    tic
    for i=1:length(xval)
       vett(i)=spline(x,y,xval(i)); 
    end
    tSpline=toc;
    
%    Rappresentare in un grafico 
%    l�errore commesso punto per punto come differenza in valore assoluto 
%    tra il polinomio interpolatore e la funzione che ha generato i dati

errLagrange=abs(yvera-yL)
errNewton=abs(yvera-newton)

figure
plot(xval,errNewton,'r')
hold on
plot(xval,errLagrange,'g')
title('Grafico errori');

fprintf('tempo lagrange=%d\n',tLagrange)
fprintf('tempo newton=%d\n',tNewton)
fprintf('tempo spline=%d\n',tSpline)
