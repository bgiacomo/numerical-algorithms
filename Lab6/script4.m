clear all
close all
clc


scelta=input('Scegliere fra funzione(1) o curva(2):\n');

if scelta==1
n=input('Scegliere n: ');
opzione=input('Punti equidistanti=1 \nChebishev=2\n');

m=1000;
xval=linspace(-1,1,m);

if (opzione==1)
x=linspace(-1,1,n);
   
end

if (opzione==2)
    for i=0:n-1
        x(i+1)=cos((1+2*i)/(2*(n+1))*pi); 
    end
end

opzione2=input('Scegliere i punti di interpolazione: 1.1/1+25x^2 \n 2.|x|\n');

y=zeros(size(x));
if opzione2==1
    for i=1:n
        y(i)=1/(1+(25*x(i).^2));
    end
    yvera=1./(1+25*xval.^2);
end
if opzione2==2
    for i=1:n
       y(i)=abs(x(i)); 
    end
    yvera=abs(xval);
end


fprintf('Calcolo con metodo Lagrange')

hold on

    tic;
    for i=1:length(xval)
       yL(i)=lagrange(x,y,xval(i));
    end
    tLagrange=toc
    figure
    plot(xval,yL)
    title('Lagrange');
 
fprintf('\nCalcolo con metodo Newton')
tic; %NEL CALCOLO DEL TEMPO CONSIDERO ANCHE IL CALCOLO DELLE DIFFERENZE
dx=differenze_div( x,y );
    for i=1:length(xval)
        newton(i)=Newton_interp(xval(i),x,dx);
    end
    tNewton=toc
    figure
    plot(xval,newton)
    title('Newton');
 
    %calcolo tempo spline
    tic
    for i=1:length(xval)
       vett(i)=spline(x,y,xval(i)); 
    end
    tSpline=toc;
    
%    Rappresentare in un grafico 
%    l�errore commesso punto per punto come differenza in valore assoluto 
%    tra il polinomio interpolatore e la funzione che ha generato i dati

errLagrange=abs(yvera-yL)
errNewton=abs(yvera-newton)

figure
plot(xval,errNewton,'r')
hold on
plot(xval,errLagrange,'g')
title('Grafico errori');

fprintf('tempo lagrange=%d\n',tLagrange)
fprintf('tempo newton=%d\n',tNewton)
fprintf('tempo spline=%d\n',tSpline)

end

if scelta==2

[x, y] = ginput;
xval=input('Intervallo xval: ');

opzione=input('Scegli algoritmo:\n1.Lagrange - 2.Newton - 3.Spline - 4.Confronto : ');

%trovo t

t(1)=0;
for i=2:length(x)
    t(i)=t(i-1)+sqrt((x(i)-x(i-1))^2+(y(i)-y(i-1))^2);
end
max=t(length(t));
t=t./max; %normalizzo la t facendo fratto il max


%Lagrange

if opzione==1 || opzione==4
    tic;
    for i=1:length(xval)
    
        xL(i)=lagrange( t,x,xval(i) ); 
        yL(i)=lagrange( t,y,xval(i) );
       
    end
    tempo_lagrange=toc;
    
    fprintf('Algoritmo di Lagrange: %d\n',tempo_lagrange);%stampa tic-toc
    
end


%Newton

if opzione ==2 || opzione==4
    tic;
    dx= differenze_div( t,x ); %Differenze divise
    dy=differenze_div(t,y);
    for i=1:length(xval)
    
        xN(i) = Newton_interp( xval(i),t, dx); 
        xN(i) = Newton_interp( xval(i),t,dy ); 
        
    end
    
   tempo_newton=toc; 

   fprintf('Algoritmo di Newton: %d\n',tempo_newton);
end


%Spline

if opzione==3 || opzione==4
    tic;
    yS=spline(t,y,xval);
    xS=spline(t,x,xval);
    tempo_spline=toc;   
    fprintf('Spline: %d\n',tempo_spline);
end
    

%Stampa
if opzione==1
    figure
    plot(xL,yL);
    title('Lagrange')
end
if opzione==2
    figure
    plot(xN,xN);
    title('Newton interp')
end
    
if opzione==3
    figure
    plot(xS,yS);
    title('Spline');
end

if opzione==4
    title('Confronto');
    plot(xL,yL,'r');
    hold on
    plot(xN,xN);
    hold on
    plot(xS,yS,'g');
end
hold on
plot(x,y,'ro')
end


