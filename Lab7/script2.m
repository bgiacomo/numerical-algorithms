clear all
close all 
clc

numero=input('Scegliere una funzione da 1 a 7\n');

if numero==1
    funzione='funzione';
    a=0;
    b=4;
end
if numero==2
    funzione='funzione2';
    a=1;
    b=2;
end
if numero==3
    funzione='funzione3';
    a=0;
    b=1;
end
if numero==4
    funzione='funzione4';
    a=-1;
    b=1;
end
if numero==5
    funzione='funzione5';
    a=2;
    b=3;
end
if numero==6
    funzione='funzione6';
    a=0;
    b=1;
end
if numero==7
    funzione='funzione7';
    a=0;
    b=2;
end


scelta=input('Scegliere il metodo composito: Simpson(1) o Trapezi(2)\n');


%Ora devo trovare l'H ottimale tramite Richardson%
I0=0;
N=1;
err=1;
contatore=0;

while(err>=10^(-5))
    if scelta==2
        I=trapezi_composita(a,b,funzione,N);
        n=1;
        p=1;
    end
    if scelta==1
        I=simpson_composita(a,b,funzione,N);
        n=2;
        p=2;
    end
    err=abs(I-I0)*(2^(n+p))/((2^(n+p))-1);
    N=N*2;
    contatore=contatore+1;
    I0=I; %confronto col precedente
end
N=N/2;

fprintf('Valore integrale: %d\n',I)
fprintf('Numero suddivisioni: %d\n',contatore-1)
fprintf('contatore: %d\n',N)







