function [ I ] = simpson( a,b,funzione )
%Simpson si ha con n=2 => ottengo 3 punti di interpolazione, con una
%funzione di secondo grado
%xM=xmedio

xM=(abs(a)+abs(b))/2;
h=(b-a)/2;

I=(h/3)*(feval(funzione,a)+4*feval(funzione,xM)+feval(funzione,b));

end

