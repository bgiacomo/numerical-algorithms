function [ I ] = simpson_composita( a,b,funzione,n)
%
%Formula di Simpson composita: n lo moltiplico per due cos� ho la garanzia
%che sia pari.
%

h=(b-a)/(2*n);


x=linspace(a,b,2*n+1);
I=0;

for i=1:n
    I=I+(h/3)*(feval(funzione,x(2*i-1))+4*feval(funzione,x(2*i))+feval(funzione,x(2*i+1)));
end

end

