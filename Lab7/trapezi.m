function [ I ] = trapezi( a,b,funzione )
%
%Con n=1 ottengo la formula dei trapezi, che mi interpola la funzione con
%una retta, infatti ottengo 2 punti di interpolazione.


I=((b-a)/2)*(feval(funzione,a)+feval(funzione,b));

end

