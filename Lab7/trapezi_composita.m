function [ I ] = trapezi_composita( a,b,funzione,n )
%Formula dei trapezi composita
%
%

h=(b-a)/n;

x=linspace(a,b,n+1);
I=(h/2)*(feval(funzione,x(1))+feval(funzione,x(2)));

for i=3:length(x)
    I=I+(h/2)*(feval(funzione,x(i-1))+feval(funzione,x(i)));
end
end

