### README ###

Assignments delivered during the course of Numerical Algorithms.

All the exercises are written in Matlab.

[More info about the course.](http://www.unibo.it/en/teaching/course-unit-catalogue/course-unit/2013/378220)

### Credits ###

* Giacomo Bartoli